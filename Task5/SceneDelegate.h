//
//  SceneDelegate.h
//  Task5
//
//  Created by SwordMac12 on 24/06/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

