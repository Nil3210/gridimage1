//
//  TouchViewController.m
//  Task5
//
//  Created by SwordMac12 on 28/06/21.
//

#import "TouchViewController.h"

@implementation TouchViewController
-(UIImageView *)internal{
    return internal;
}
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        internal=[[UIImageView alloc]initWithFrame:self.bounds];
        [internal setBackgroundColor:[UIColor blackColor]];
        [internal setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:internal];
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{\
    if(isLarge)[self makeSmall];
    else [self makeFull];
}
-(void)makeFull
{
    [[self superview]bringSubviewToFront:self];
    isLarge=YES;
    CGRect largeFrame=[self superview].bounds;
    Original=self.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [self setFrame:largeFrame];
    [internal setFrame:self.bounds];
    [UIView commitAnimations];
}
-(void)makeSmall
{
    isLarge=NO;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [self setFrame:Original];
    [internal setFrame:self.bounds];
    [UIView commitAnimations];
  
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
