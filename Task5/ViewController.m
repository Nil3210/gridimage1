//
//  ViewController.m
//  Task5
//
//  Created by SwordMac12 on 24/06/21.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    int rows=10,columns=3;
    CGSize blockSize = CGSizeMake(self.view.frame.size.width/columns, self.view.frame.size.height/rows);
    for(int y=0;y<rows;y++){
        for(int x=0;x<columns;x++){
            CGPoint blockOrigin = CGPointMake(x*blockSize.width, y*blockSize.height);
            CGRect blockFrame;
            blockFrame.size=blockSize;
            blockFrame.origin=blockOrigin;
            UIImage * blockImage=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://miro.medium.com/max/1400/1*xj28wQAoHyc-WSzDImPMNQ.jpeg"]]];
            TouchViewController *tiv =[[TouchViewController alloc] initWithFrame:blockFrame];
            [[tiv internal]setImage:blockImage];
            [self.view addSubview:tiv];
            
        }
    }
}


@end
