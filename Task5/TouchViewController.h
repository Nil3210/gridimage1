//
//  TouchViewController.h
//  Task5
//
//  Created by SwordMac12 on 28/06/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TouchViewController : UIView
{
    UIImageView * internal;
    CGRect Original;
    BOOL isLarge;
}
-(UIImageView *)internal;
-(void)makeFull;
-(void)makeSmall;

@end

NS_ASSUME_NONNULL_END
